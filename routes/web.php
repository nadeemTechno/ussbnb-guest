<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});


$router->group(['prefix' => '/api/v1'], function() use ($router) {
	//Guests
	$router->get('/guests', array('as'=>'Guest List', 'uses'=>'GuestController@guests'));
    $router->get('/guests/{id}', array('as'=>'Guest Details', 'uses'=>'GuestController@getGuestDetail'));
    $router->post('/guests', array('as'=>'Create Guest', 'uses'=>'GuestController@createGuest'));
    $router->delete('/guests/{id}', array('as'=>'Delete Guest', 'uses'=>'GuestController@deleteGuest'));
    $router->put('/guests/{id}', array('as'=>'Guest Update', 'uses'=>'GuestController@updateGuest'));


	$router->post('/findGuestByAttr', array('as'=>'Guest Details', 'uses'=>'GuestController@findGuestByAttr'));
    $router->get('/getEditGuestDetail/{id}', array('as'=>'Guest Edit', 'uses'=>'GuestController@getEditGuestDetail'));


});